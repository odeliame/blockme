package logics;

/**
 * represent a _move of one _block
 */
public class Move {
    private Block _block;
    // true - right or down, false - up or left
    private boolean _move;

    //constructor
    public Move(Block block, boolean move) {
        this._block = block;
        this._move = move;
    }

    //getter and setters
    public Block getBlock() {
        return _block;
    }

    public boolean isMove() {
        return _move;
    }
}
