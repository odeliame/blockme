package logics;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.HashMap;

/**
 * data class - responsble for data fetching and updating
 *
 */
public class Data {

    private static String filepath= "";

    public static void initLevelFile(){
        Data.filepath = System.getProperty("user.dir")+"/levels.json";
        File newJsonFile = new File(Data.filepath);
        if (!newJsonFile.exists()) {
            try {
                Type type = new TypeToken<HashMap<Integer, Level>>() {
                }.getType();
                Gson gson = new Gson();
                InputStream in = Data.class.getResourceAsStream("/Levels/levels.json");
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                HashMap<Integer, Level> levelHashMap = gson.fromJson(reader, type);
                String jsonString = gson.toJson(levelHashMap);
                newJsonFile.createNewFile();
                BufferedWriter writer = new BufferedWriter(new FileWriter(newJsonFile));
                writer.write(jsonString);
                writer.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }


    /**
     * returns the last laval index in the current saved levels hashmap
     * @return
     */
    public static int getLastLevelIndex(){
        HashMap<Integer,Level> levelHashMap = loadLevels();
        return Collections.max(levelHashMap.keySet());
    }

    /**
     * delete level from the current level hashmap -
     * including move all other level to the empty space if needed.
     * @param id
     */
    public static void deleteLevel(int id){
        HashMap<Integer,Level> levelHashMap = loadLevels();
        levelHashMap.remove(id);
        for (int key : levelHashMap.keySet()){
            if (key>id) {
                Level tmp = levelHashMap.get(key);
                tmp.setLevelId(key-1);
                levelHashMap.remove(key);
                levelHashMap.put(tmp.getLevelId(), tmp);
            }
        }
        writeLevels(levelHashMap);
    }

    /**
     * loads all the levels
     * @return
     */
    public static HashMap<Integer,Level> loadLevels() {
        try {
            Type type = new TypeToken<HashMap<Integer, Level>>() {
            }.getType();
            Gson gson = new Gson();
            BufferedReader reader = new BufferedReader(new FileReader(Data.filepath));
            HashMap<Integer, Level> levels = gson.fromJson(reader, type);
            return levels;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * writes all the level hashmap to a json file
     * @param levels
     */
    private static void writeLevels(HashMap<Integer,Level> levels) {
        try {
            Gson g = new Gson();
            String jsonstring = g.toJson(levels);
            BufferedWriter writer = new BufferedWriter(new FileWriter(Data.filepath));
            writer.write(jsonstring);
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * saves new level to the file
     * (not used for update exciting one)
     * @param level
     */
    public static void saveNewLevel(Level level) {
        HashMap<Integer, Level> allLevels = loadLevels();
        allLevels.put(level.getLevelId(), level);
        writeLevels(allLevels);
    }

    /**
     * update best time of existing level - when level finished
     * @param level
     * @param bestTime
     */
    public static void updateBestTime(Level level,int bestTime){
        HashMap<Integer, Level> allLevels = loadLevels();
        Level updated = allLevels.get(level.getLevelId());
        updated.setBestTime(bestTime);
        allLevels.put(updated.getLevelId(),updated);
        writeLevels(allLevels);

    }

    /**
     * loads specific level with given id
     * @param id - the id of the desired level
     * @return
     */
    public static Level loadLevel(int id){
        HashMap<Integer,Level> levels = loadLevels();
        if(levels.keySet().contains((Integer)id)) {
            return levels.get(id);
        }
        return null;
    }


}
