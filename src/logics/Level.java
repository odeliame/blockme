package logics;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * Created by user on 17/05/2016.
 */
public class Level {
    int levelID;
    private List<Block> blocks;
    private int bestTime;

    //constructors
    public Level(List<Block> blocks, int bestTime, int levelID) {
        this.blocks = blocks;
        this.bestTime = bestTime;
        this.levelID = levelID;
    }

    public Level(Block[][] blocksArr, int levelID){
        this.levelID = levelID;
        this.bestTime = Integer.MAX_VALUE;
        this.blocks = new ArrayList<Block>();

        for (int i = 0; i < blocksArr.length; i++) {
            for (int j = 0; j < blocksArr[i].length; j++) {
                if (blocksArr[i][j]!=null){
                    if (!blocks.contains(blocksArr[i][j])){
                        blocks.add(blocksArr[i][j]);
                    }
                }
            }

        }

    }

    public Level(int levelID, List<Block> blocks) {
        this.levelID = levelID;
        blocks.add(Block.generateTargetBlock());
        this.blocks = blocks;
        this.bestTime = Integer.MAX_VALUE;

    }

    public Level(int levelID) {
        this.bestTime = Integer.MAX_VALUE;
        this.blocks = new Vector<Block>() ;
        this.blocks.add(Block.generateTargetBlock());
        this.levelID = levelID;
    }

    //clone a level
    public Level clone(){
        Level lvl = new Level(this.levelID);
        lvl.blocks = new Vector<Block>();
        for (Block block :this.blocks  ) {
            lvl.blocks.add(block.clone());
        }
        lvl.bestTime = this.bestTime;
        return lvl;
    }

    //setter-getters
    public void setBestTime(int newbBestTime){
        if (newbBestTime<bestTime) {
            this.bestTime = newbBestTime;
        }
    }

    public void setLevelId(int levelID) {
        this.levelID = levelID;
    }

    public List<Block> getBlocks() {
        return blocks;
    }
    public int getLevelId(){
        return levelID;
    }
    public int getBestTime(){
        return bestTime;
    }
}
