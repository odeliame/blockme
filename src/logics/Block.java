package logics;

/**
 * Created by user on 17/05/2016.
 */
public class Block {
    boolean isVertical, isTarget;
    int x, y, size;

    public Block(boolean isVertical, int x, int y, int size,boolean isTarget) {
        this.isVertical = isVertical;
        this.isTarget = isTarget;
        this.x = x;
        this.y = y;
        this.size = size;
    }

    public Block(boolean isVertical, int x, int y, int size) {
        this.isVertical = isVertical;
        this.isTarget = false;
        this.x = x;
        this.y = y;
        this.size = size;
    }

    public Block clone(){
        Block b = new Block(this.isVertical(),this.getX(),this.getY(), this.getSize(), this.isTarget());
        return b;
    }

    public static Block generateTargetBlock(){
        return new Block(false,0,2,2,true);
    }

    public boolean isVertical() {
        return isVertical;
    }

    public void setVertical(boolean vertical) {
        isVertical = vertical;
    }

    public boolean isTarget() {
        return isTarget;
    }

    public void setTarget(boolean target) {
        isTarget = target;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }
}
