package logics;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * represent a logic game
 */
public class PlayGame {

    public static final int boardSize = 6;
    private Level _level;
    private Stack<Move> _moves;
    private Block[][] _board;
    private Block _targetBlock;

    //constructor
    public PlayGame(Level level) {
        this._level = level;
        this._moves = new Stack<>();
        this._board =  getBoardFromLevel();

    }
    /**
     * Generate blocks array from a given _level,
     * puts the cars in the right place on it.
     *
     * @return
     */
    public Block[][] getBoardFromLevel(){
        Block[][] resboard = new Block[6][6];
        for (Block b : _level.getBlocks()) {
            if (!b.isVertical()) {
                for (int i = 0; i < b.getSize(); i++) {
                    resboard[b.getY()][b.getX() + i] = b;
                }
            } else {
                for (int i = 0; i < b.getSize(); i++) {
                    resboard[b.getY() + i][b.getX()] = b;
                }
            }
            if (b.isTarget()) {
                _targetBlock = b;
            }

        }
        return resboard;
    }

    /**
     * load a _level and create a game according to it
     *
     * @param levelID
     * @return
     */
    public static PlayGame loadGame(int levelID) {
        Level level = Data.loadLevel(levelID);
        return new PlayGame(level);
    }
    /**
     * update _level best time
     *
     * @param recordtime
     */
    public void saveLevel(int recordtime) {
        this._level.setBestTime(recordtime);
        //update on data file
        Data.updateBestTime(this._level, recordtime);

    }


    //region getters-setters
    public static int getBoardSize() {
        return boardSize;
    }

    public Level getLevel() {
        return _level;
    }

    public void setLevel(Level level) {
        this._level = level;
    }

    public Stack<Move> getMoves() {
        return _moves;
    }

    public void setMoves(Stack<Move> moves) {
        this._moves = moves;
    }

    public Block[][] getBoard() {
        return _board;
    }

    public void setBoard(Block[][] board) {
        this._board = board;
    }

    public Block getTargetBlock() {
        return _targetBlock;
    }

    public void setTargetBlock(Block targetBlock) {
        this._targetBlock = targetBlock;
    }
    //endregion

    /**
     * check if the car can preform a specific move-
     * if she doesn't hit walls or any other car
     *
     * @param block - the one want to move
     * @param move  - up+left - false, down+right - true
     * @return
     */
    private boolean canMove(Block block, boolean move) {
        if (!block.isVertical()) {
            //right
            if ((move && block.getX() + block.getSize() < boardSize && isPositionCleared(block.getY(), block.getX() + block.getSize()))
                    //left
                    || (!move && block.getX() - 1 >= 0 && isPositionCleared(block.getY(), block.getX() - 1))) {
                return true;
            }
        }
        //down
        else if ((move && block.getY() + block.getSize() < boardSize && isPositionCleared(block.getY() + block.getSize(), block.getX()))
                //up
                || (!move && block.getY() - 1 >= 0 && isPositionCleared(block.getY() - 1, block.getX()))) {
            return true;
        }
        return false;

    }
    /**
     * _moves the block - update _board and the block's positions
     *
     * @param block
     * @param move
     */
    private void moveBlock(Block block, boolean move) {
        if (!block.isVertical()) {
            //move right
            if (move) {
                _board[block.getY()][block.getX()] = null;
                _board[block.getY()][block.getX() + block.getSize()] = block;
                block.setX(block.getX() + 1);
            } else {
                //move left
                _board[block.getY()][block.getX() - 1] = block;
                _board[block.getY()][block.getX() + block.getSize() - 1] = null;
                block.setX(block.getX() - 1);
            }
        } else {
            if (move) {
                //move down
                _board[block.getY() + block.getSize()][block.getX()] = block;
                _board[block.getY()][block.getX()] = null;
                block.setY(block.getY() + 1);
            } else {
                //move up
                _board[block.getY() + block.getSize() - 1][block.getX()] = null;
                _board[block.getY() - 1][block.getX()] = block;
                block.setY(block.getY() - 1);

            }
        }
        _moves.add(new Move(block, move));
    }
    /**
     * check if a piont on the _board is cleared
     *
     * @param y -_y rate
     * @param x -_x rate
     * @return
     */
    public boolean isPositionCleared(int y, int x) {
        if (_board[y][x] == null)
            return true;
        return false;
    }
    //undo the last move made
    public Block undoMove() {
        Move move = _moves.pop();
        Block b = move.getBlock();
        moveBlock(b, !move.isMove());
        _moves.pop();
        return b;
    }
    //doing a specific move
    public Block redoMove(Move move){
        Block b = move.getBlock();
        moveBlock(b, move.isMove());
        return b;
    }
    //move the block if no other car is in the way
    public Block[][] moveIfAble(Block block, boolean move) {
        if (canMove(block, move)) {
            moveBlock(block, move);
        }
        return _board;
    }
    //check if game is finished
    public boolean isSolved() {
        if (_targetBlock.getX() == 4 && _targetBlock.getY() == 2) {
            return true;
        }
        return false;
    }

    //*****hint algorithm part*******


    /**
     * move is able in a certain direction,
     * until the given point is cleared
     *
     * @param problem- the block moving
     * @param point    - the point he need to clear
     * @param move     - the direction
     * @return
     */
    public boolean tryMove(Block problem, Point point, boolean move) {
        while (canMove(problem, move)) {
            _board = moveIfAble(problem, move);
            if (isPositionCleared(point.Y, point.X)) {
                return true;
            }
        }
        return false;

    }
    /**
     * trying to clear a point in all possible directions
     * if fails, return a list of all the other points needed to
     * be cleared for the current moving to complete.
     *
     * @param problem - the moving block
     * @param point   - the current point to clear
     * @return
     */
    public List<Point> getProblemPoints(Block problem, Point point) {
        List<Point> points = new ArrayList();
        Stack<Move> tries = (Stack<Move>) this._moves.clone();
        if (!problem.isVertical()) {
            //try right
            if (tryMove(problem, point, true)) {
                return new ArrayList();
            } else {

                if (problem.getX() + problem.getSize() < boardSize) {
                    points.add(new Point(problem.getX() + problem.getSize(), problem.getY()));
                }
                reverseAllTheTries(tries);
            }
            //try left
            if (tryMove(problem, point, false)) {
                return new ArrayList();
            } else {
                if (problem.getX() - 1 >= 0) {
                    points.add(new Point(problem.getX() - 1, problem.getY()));
                }
                reverseAllTheTries(tries);

            }
        } else {
            //try down
            if (tryMove(problem, point, true)) {
                return new ArrayList();
            } else {
                if (problem.getY() + problem.getSize() < boardSize) {
                    points.add(new Point(problem.getX(), problem.getY() + problem.getSize()));
                }
                reverseAllTheTries(tries);
            }
            //try up
            if (tryMove(problem, point, false)) {
                return new ArrayList();
            } else {

                if (problem.getY() - 1 >= 0) {
                    points.add(new Point(problem.getX(), problem.getY() - 1));
                }
                reverseAllTheTries(tries);
            }
        }
        return points;
    }
    //reverse all failed tries - undo everything done wrong
    private void reverseAllTheTries(Stack<Move> tries) {
        while (!tries.equals(this._moves)) {
            undoMove();
        }
    }
    /**
     * Try to clear a point - recursive
     * if fails, get the problematic points that made it fail
     * and trying to clear them.
     *
     * @param point
     * @return
     */
    public boolean clearPoint(Point point) {
        if (isPositionCleared(point.Y, point.X)) {
            return true;
        }

        Block problem = _board[point.Y][point.X];
        List<Point> problemPoints = getProblemPoints(problem, point);
        int numOfTries = 0;
        if (problemPoints.isEmpty()){return true;}
        while (!problemPoints.isEmpty() && numOfTries < 5) {
            for (Point p : problemPoints) {
                if (clearPoint(p)) {
                    break;
                }
            }
            problemPoints = getProblemPoints(problem, point);
            if (!problemPoints.isEmpty()) {
                numOfTries += 1;
            } else return true;
        }

        return false;
    }
    /**
     * clearing all point that seperates
     * target block from the door out!
     *
     * @return
     */
    public boolean solve() {
        boolean isSucceeded = false;
        for (int i = _targetBlock.getX() + _targetBlock.getSize(); i < 6; i++) {
            isSucceeded = clearPoint(new Point(i, _targetBlock.getY()));
            if (!isSucceeded) { return false;  }
            else{
                moveBlock(_targetBlock, true); }
        }

        return true;
    }
    /**
     * calling for trying to solve,
     * and returns to state after only first move
     *
     * @return
     */
    public boolean giveHint() {
        int currentMoveSize = this._moves.size();
        if(isSolved()){return false;}
        if (solve()) {
            optimizeMoves(currentMoveSize);
            return true;
        }
        return false;
    }
    /**
     * check if a specific _board is exist in history-
     * detecting loops - _moves made, and then reversed
     *
     * @param history
     * @param board
     * @return
     */
    public int historyContains(List<Block[][]> history, Block[][] board){
        if (history.size()==0){return -1;}
        for (Block[][] hist:history  ) {
            if (isEqual(hist, board)){
                return history.indexOf(hist);
            }
        }
        return -1;
    }
    //check if two boards are equal - only by empty/full positions.
    public boolean isEqual(Block[][] board1 , Block[][] board2) {
        for (int i = 0; i < board1.length; i++) {
            for (int j = 0; j < board1[i].length; j++) {
                if ((board1[i][j]!=null&&board2[i][j]==null)||
                        (board1[i][j]==null&&board2[i][j]!=null))
                {return false;}
            }
        }
        return true;
    }
    /**
     * detecting loops starting from beginnings state,
     * until solved stated - if loops found == a state that is the same
     * as the beginnings state - brings the _board to the position of last
     * occurrence of it.
     *
     * @param currentMoveSize
     * @return
     */
    public int optimizeMoves(int currentMoveSize) {
        List<Block[][]> history = new ArrayList<>();
        Stack<Move> undoedMoves = new Stack<>();
        while (this._moves.size()>currentMoveSize) {
            history.add(getBoardFromLevel());
            undoedMoves.push(_moves.peek());
            undoMove();
        }
        int circle = historyContains(history, this._board);
        if (circle!=-1) {
            for (int i = 0; i < history.size() -circle; i++) {
                undoedMoves.pop();
            }
            redoMove(undoedMoves.pop());

        }
        else { redoMove(undoedMoves.pop());}

        return circle;

//        circle = historyContains(history.subList(1,history._size()), history.get(history._size()-1));
//        if (circle!=-1) {
//            for (int i = 0; i < history._size() -circle + 2; i++) {
//                redoMove(undoedMoves.pop());
//            }
//        }

    }
}
