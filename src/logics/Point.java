package logics;

/**
 * represent a point on the board
 */
public class Point {
    public final int X;
    public final int Y;

    public Point(int x, int y) {
        X = x;
        Y = y;
    }
}
