package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import GraphicsCore.Game;
import GraphicsCore.levelSmallBlock;
import logics.Data;

public class menuListener implements ActionListener,MouseListener{
	
	private JButton _play;
	private JButton _createLevels;
	private JButton _mainMenu;
	private JButton _chooseLevel;
	private JButton _nextLevel;
	private JButton _exit;
	private Game _frame;
	public menuListener(JButton mainMenu, JButton chooseLevel, JButton nextLevel, Game frame) {
		_frame = frame;
		_mainMenu=mainMenu;
		_chooseLevel=chooseLevel;
		_nextLevel=nextLevel;
	}	
	public menuListener(JButton play, JButton createLevels, Game frame) {
		_frame = frame;
		_play=play;
		_createLevels=createLevels;
	}
	public menuListener(Game frame) {
		_frame = frame;
		_play=null;
		_createLevels=null;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==_play){
			_frame.setChoosePanelWindow();
		}
		else if (e.getSource()==_createLevels){
			_frame.setNewLevel();
		}
		else if (e.getSource()==_exit){
			System.exit(0);
		}
		else if (e.getSource()==_mainMenu){
			_frame.setMenuWindow();
		}
		else if (e.getSource()==_chooseLevel){
			_frame.setChoosePanelWindow();
		}
		else if (e.getSource()==_nextLevel){
			if (_frame.getPlayGame().getLevel().getLevelId()>=Data.getLastLevelIndex())
				_frame.setPlayingGame(0);
			else _frame.setPlayingGame(_frame.getPlayGame().getLevel().getLevelId()+1);
		}
			
		
	}
	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getButton() == MouseEvent.BUTTON3){
			if (e.getSource() instanceof levelSmallBlock){
				int levelId = ((levelSmallBlock) e.getSource()).getLevelId();
				if (levelId != -2){
					Data.deleteLevel(levelId);
					_frame.setChoosePanelWindow();
				}
				else _frame.setNewLevel();
				//delete
			}
		}
		else 
			if (e.getSource() instanceof levelSmallBlock){
				int levelId = ((levelSmallBlock) e.getSource()).getLevelId();
				if (levelId != -2)
					_frame.setPlayingGame(levelId);
				else _frame.setNewLevel();
			}		
	}
	@Override
	public void mouseEntered(MouseEvent e) {
		if (e.getSource() instanceof levelSmallBlock){
			try {
				int levelId = ((levelSmallBlock) e.getSource()).getLevelId();
				if (levelId == -2){
					((levelSmallBlock) e.getSource()).changeImage(ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSbCg.jpg")));
				}
				else 
					((levelSmallBlock) e.getSource()).changeImage(ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSbGreen.jpg")));
			} catch (IOException e1) {
				throw new RuntimeException("unable to change menu image");
			}
		}
	}
	@Override
	public void mouseExited(MouseEvent e) {
		if (e.getSource() instanceof levelSmallBlock){
			try {
				((levelSmallBlock) e.getSource()).changeImage(ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSB.jpg")));
			} catch (IOException e1) {
				throw new RuntimeException("unable to rechange menu image");
			}
		}
	}
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	public void setExitButton(JButton _exit) {
		this._exit = _exit;
	}
	
}
