package Listeners;

import GraphicsCore.Board;
import GraphicsCore.CreatePanel;
import GraphicsCore.Game;
import GraphicsCore.smallBlock;
import logics.Block;
import logics.Data;
import logics.Level;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class levelBuildingListener implements MouseListener, ActionListener{
    JButton _peakmator;
    JButton _peakMack;
    JButton _peakmatorV;
    JButton _peakMackV;
    JButton _deleteFromBoard;
    JButton _doneButton;
    Board _board;
    CreatePanel createPanel;
    Block _notplaced;
    private Game _game;
    public levelBuildingListener(Game game){
    	_game=game;
    }


    //region gettersetter
    public void setCreatePanel(CreatePanel createPanel) {
        this.createPanel = createPanel;
    }

    public JButton get_peakMackV() {
        return _peakMackV;
    }

    public void set_peakMackV(JButton _peakMackV) {
        this._peakMackV = _peakMackV;
    }

    public JButton get_peakmatorV() {
        return _peakmatorV;
    }

    public void set_peakmatorV(JButton _peakmatorV) {
        this._peakmatorV = _peakmatorV;
    }

    public void set_board(Board _board) {
        this._board = _board;
    }

    public void set_peakmator(JButton _peakmator) {
        this._peakmator = _peakmator;
    }

    public void set_peakMack(JButton _peakMack) {
        this._peakMack = _peakMack;
    }

    public void set_deleteFromBoard(JButton _deleteFromBoard) {
        this._deleteFromBoard = _deleteFromBoard;
    }

    public void set_doneButton(JButton _doneButton) {
        this._doneButton = _doneButton;
    }
    //endregion

    //region unneeded
    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
    //endregion

    @Override
    public void mouseClicked(MouseEvent e) {
        smallBlock b = ((smallBlock) e.getSource());
        boolean marked = b.isMarked();
        b.mark(!marked);

        if (_notplaced != null) {
            _notplaced.setY(b.getRow());
            _notplaced.setX(b.getCol());

            putCarOnBoard();
        }
        _notplaced= null;
    }

    public void putCarOnBoard(){
        Block[][] blocks = _board.getLogicBoard();

        if (!_notplaced.isVertical()) {
            for (int i = 0; i < _notplaced.getSize(); i++) {
                blocks[_notplaced.getY()][_notplaced.getX() + i] = _notplaced;
            }
        } else {
            for (int i = 0; i < _notplaced.getSize(); i++) {
                blocks[_notplaced.getY() + i][_notplaced.getX()] = _notplaced;
            }
        }
        _board.removeAll();
        _board.revalidate();
        _board.createBoard(blocks,this);

    }

    public void deleteCarFromBoard(Block car){
        Block[][] blocks = _board.getLogicBoard();

        if (!car.isVertical()) {
            for (int i = 0; i < car.getSize(); i++) {
                blocks[car.getY()][car.getX() + i] = null;
            }
        } else {
            for (int i = 0; i < car.getSize(); i++) {
                blocks[car.getY() + i][car.getX()] = null;
            }
        }
        _board.removeAll();
        _board.revalidate();
        _board.createBoard(blocks,this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(_deleteFromBoard)){
            smallBlock b =_board.getClickedBlock();
            if (b.getBlock().isTarget()){return;}
            deleteCarFromBoard(b.getBlock());
            return;
        }

        else if(e.getSource().equals(_peakMack)){
            _notplaced=new Block(false,0,0,3);
        }

        else if(e.getSource().equals(_peakMackV)){
            _notplaced=new Block(true,0,0,3);
        }

        else if(e.getSource().equals(_peakmator)){
            _notplaced=new Block(false,0,0,2);
        }

        else if(e.getSource().equals(_peakmatorV)){
            _notplaced=new Block(true,0,0,2);
        }

        else if (e.getSource().equals(_doneButton)){
            Level newlevel = new Level(_board.getLogicBoard(), Data.getLastLevelIndex()+1);
            Data.saveNewLevel(newlevel);
            _game.setMenuWindow();
            
        }

    }


}
