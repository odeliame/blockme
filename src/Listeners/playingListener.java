package Listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import GraphicsCore.Board;
import GraphicsCore.ControlPanel;
import GraphicsCore.Game;
import GraphicsCore.smallBlock;
import logics.Block;

public class playingListener implements MouseListener,KeyListener,ActionListener{
	private Board _board;
	private ControlPanel _cp;
	private JButton _back;
	private JButton _undo;
	private JButton _hint;
	
	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_LEFT){
			if (_board.getClickedSmallBlock()!=null){
				if (!_board.isFirstMovePlayed()){
					_board.setFirstMovePlayed(true);
					_cp.startTheTimer();
				}
				if (_board.getClickedSmallBlock().getBlock()!=null)
					if (!_board.getClickedSmallBlock().getBlock().isVertical()){
						_board.getPlayGame().moveIfAble(_board.getClickedSmallBlock().getBlock(),false);
						updateAndMark();
					}
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_RIGHT){
			if (_board.getClickedSmallBlock()!=null){
				if (!_board.isFirstMovePlayed()){
					_board.setFirstMovePlayed(true);
					_cp.startTheTimer();
				}

				if (_board.getClickedSmallBlock().getBlock()!=null){
					if (!_board.getClickedSmallBlock().getBlock().isVertical()){
						_board.getPlayGame().moveIfAble(_board.getClickedSmallBlock().getBlock(),true);
						updateAndMark();
					if (_board.getClickedSmallBlock().getBlock().isTarget() && _board.getClickedSmallBlock().getCol()==4 && _board.getClickedSmallBlock().getRow()==2)
						_cp.won();
					}
				}
				
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_UP){
			if (_board.getClickedSmallBlock()!=null){
				if (!_board.isFirstMovePlayed()){
					_board.setFirstMovePlayed(true);
					_cp.startTheTimer();
				}
				if (_board.getClickedSmallBlock().getBlock()!=null)
					if (_board.getClickedSmallBlock().getBlock().isVertical()){
						_board.getPlayGame().moveIfAble(_board.getClickedSmallBlock().getBlock(),false);
						updateAndMark();
					}
			}
		}
		if (e.getKeyCode() == KeyEvent.VK_DOWN){
			if (_board.getClickedSmallBlock()!=null){
				if (!_board.isFirstMovePlayed()){
					_board.setFirstMovePlayed(true);
					_cp.startTheTimer();
				}
				if (_board.getClickedSmallBlock().getBlock()!=null)
					if (_board.getClickedSmallBlock().getBlock().isVertical()){
						_board.getPlayGame().moveIfAble(_board.getClickedSmallBlock().getBlock(),true);
						updateAndMark();
					}
			}
		}

	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseClicked(MouseEvent arg0) {
		boolean marked=((smallBlock) arg0.getSource()).isMarked();
		((smallBlock) arg0.getSource()).mark(!marked);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(_back)){
			_cp.getGame().setMenuWindow();
		}
		if (e.getSource().equals(_undo)){
			if (_board.isFirstMovePlayed()){
				Block b=_board.getPlayGame().undoMove();
				_board.setClickedSmallBlock(null);
				_board.updateBoard();
				if (b!=null)
					_board.getSmallBlocks()[b.getY()][b.getX()].mark(true);
			}

		}
		if (e.getSource().equals(_hint)){
			boolean played = _board.getPlayGame().giveHint();
			if (played){
				if (!_board.isFirstMovePlayed()){
					_board.setFirstMovePlayed(true);
					_cp.startTheTimer();
				}
				updateAndMark();
				if (_board.getClickedSmallBlock().getBlock().isTarget() && _board.getClickedSmallBlock().getCol()==4 && _board.getClickedSmallBlock().getRow()==2)
					_cp.won();
			}
		}
		
	}
	private void updateAndMark(){
		_board.updateBoard();
		Block played = _board.getPlayGame().getMoves().peek().getBlock();
		if (played!=null)
			_board.getSmallBlocks()[played.getY()][played.getX()].mark(true);
	}
	public JButton getBack() {
		return _back;
	}

	public void setBack(JButton back) {
		this._back = back;
	}

	public JButton getUndo() {
		return _undo;
	}

	public void setUndo(JButton undo) {
		this._undo = undo;
	}

	public JButton getHint() {
		return _hint;
	}

	public void setHint(JButton hint) {
		this._hint = hint;
	}
	public void setMainFrame(Game mainFrame){
		_board=mainFrame.getBoard();
		_cp=mainFrame.getControlPanel();
	}
}
