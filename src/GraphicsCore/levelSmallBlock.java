package GraphicsCore;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;

import logics.Level;

public class levelSmallBlock extends JPanel {
	
	private int _levelId;
	private int _bestTime;
	private BufferedImage _image;
	
	public levelSmallBlock(Level level, MouseListener listener){
		setLayout(new BoxLayout(this,BoxLayout.Y_AXIS));
		addMouseListener(listener);
		JLabel levelId = new JLabel("Empty                 ");
		JLabel timeLabel = new JLabel("Spot        ");
		if (level!=null){ //if such level exist - not an empty slot
			_levelId=level.getLevelId();
			_bestTime =level.getBestTime();
			Integer shortBestTime = new Integer(_bestTime);

			if (!(_bestTime >0 && _bestTime <9999999)){
				levelId = new JLabel("level is");
				timeLabel = new JLabel ("not finished");
			}
			else {
				levelId = new JLabel("Level: "+_levelId);
				timeLabel = new JLabel ("Best Time: "+shortBestTime.shortValue());
			}
		}
		else {
			_levelId=-2; //if its empty slot
		}
		add(levelId,BorderLayout.CENTER);
		add(timeLabel,BorderLayout.CENTER);
		try {
			_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSB.jpg"));
		} 
		catch (IOException e) {
			throw new RuntimeException("unable to load menu _image");
		}
		this.setSize(100, 100);
	    setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setVisible(true);
	}
	
	//change the background _image;
	public void changeImage(BufferedImage image){
		_image=image;
		repaint();
	}
	//putting a background _image for the JPanel
	public void paintComponent(Graphics g){
		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);
	}
	public int getLevelId(){
		return _levelId;
	}
	
	
}
