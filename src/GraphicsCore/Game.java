package GraphicsCore;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import Listeners.levelBuildingListener;
import Listeners.playingListener;
import logics.Block;
import logics.PlayGame;

import static logics.Data.initLevelFile;

public class Game extends JFrame{
	private GridBagLayout _gbl;
	private GridBagConstraints _gblC2;
	private Board _theBoard;
	private ControlPanel _ControlPanel;
	private CreatePanel _createPanel;
	private JPanel _ContentPanel;
	private PlayGame _level;
	
	public Game(){
		initLevelFile();
		pack();
		setLocationByPlatform(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setTitle("Welcome");
		this.setSize(600, 660);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	//sets a menu window
	public void setMenuWindow(){
		if (_ContentPanel!=null)
			_ContentPanel.removeAll();
		Menu menu = new Menu(this);
		setContentPane(menu);
	}
	
	//sets a level choosing window
	public void setChoosePanelWindow(){
		if (_ContentPanel!=null){
			
		}
		chooseLevel chooseLevel = new chooseLevel(this);
		setContentPane(chooseLevel);
	}
	
	//sets a winning page if mission completed
	public void setWinPage(int playedTime,int bestTime){
		winningPage winningPage = new winningPage(this,playedTime,_level.getLevel());
		setContentPane(winningPage);
	}
	
	//sets a playing game window
	public void setPlayingGame(int id){
		_level = logics.PlayGame.loadGame(id);
		playingListener listener = new playingListener();
		_theBoard = new Board(this,listener,listener);
		_ControlPanel=new ControlPanel(this,listener);
		
		listener.setMainFrame(this);
		
		_gbl = new GridBagLayout();
		_gblC2=new GridBagConstraints();
		_ContentPanel = new JPanel();
		_ContentPanel.setLayout(_gbl);
		_ContentPanel.setSize(600, 630);

		addPanel(_ControlPanel,0,0,0,0.05);
		addPanel(_theBoard,0,1,1,0.95);
		setContentPane(_ContentPanel);
		newKeyListener(listener);
	    
	}
	
	//remote all current key listeners in order to avoid collisions
	private void newKeyListener(KeyListener listener){
		setFocusable(true);
		requestFocus();
	    for( KeyListener al : getKeyListeners() ) {
	        removeKeyListener( al );
	    }
	    this.addKeyListener(listener);
	}
	
	//sets a create level window
	public void setNewLevel(){
		levelBuildingListener listener =new levelBuildingListener(this);

		//builds a empty board with target block
		Block[][] board = new Block[6][6];
		Block target= Block.generateTargetBlock();
		board[2][0] = target;
		board[2][1] = target;

		_theBoard = new Board(board, listener);
		_createPanel=new CreatePanel(listener);
		listener.setCreatePanel(_createPanel);
		listener.set_board(_theBoard);

		_gbl = new GridBagLayout();
		_gblC2=new GridBagConstraints();
		_ContentPanel = new JPanel();
		_ContentPanel.setLayout(_gbl);
		_ContentPanel.setSize(600, 630);

		addPanel(_createPanel,0,0,0,0.05);
		addPanel(_theBoard,0,1,1,0.95);
		setContentPane(_ContentPanel);

		this.setFocusable(true);
		this.requestFocus();
	}

	//adding a panel to the content panel - used if we want to show few panels in a frame with gridbaglayout
	private void addPanel(JPanel panel,int gridx,int gridy, double weightx, double weighty){
		_gblC2.gridx = gridx;
		_gblC2.gridy = gridy;
		_gblC2.gridwidth = 1;
		_gblC2.gridheight = 1;
		_gblC2.fill = GridBagConstraints.BOTH;
		_gblC2.weightx = weightx;
		_gblC2.weighty = weighty;
		_ContentPanel.add(panel,_gblC2);
	}
	public void setNewMainPanel(JPanel panel, int gridx,int gridy, double weightx, double weighty){
		_ContentPanel.removeAll();
		_ContentPanel.revalidate();
		addPanel(panel,gridx,gridy,weightx,weighty);
	}

	public PlayGame getPlayGame(){
		return _level;
	}
	public Board getBoard(){
		return _theBoard;
	}
	public ControlPanel getControlPanel(){
		return _ControlPanel;
	}

}
