package GraphicsCore;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.Timer;
import Listeners.playingListener;

public class ControlPanel extends JPanel{

	private JLabel _timePassedLabel;
	private JButton _back;
	private JButton _undo;
	private JButton _hint;
	private Timer _t;
	private Game _mainFrame;
	private int _timepassed=0;
	private BufferedImage _image;

	public ControlPanel(Game frame, playingListener listener){
		_mainFrame=frame;
		this.setSize(600,25);
		setLayout(new FlowLayout());
		_t = new Timer(1000,new ActionListener(){
			public void actionPerformed(ActionEvent e) {
					_timepassed+=1;
					_timePassedLabel.setText("Time Passed: "+_timepassed);
				
			}
		});


		 ImageIcon backIcon=new ImageIcon(getClass().getResource("/Images/back.jpg"));
		 _back = putIconOnButtton(backIcon,2);
		listener.setBack(_back);
		_back.addActionListener(listener);
		
		 ImageIcon undoIcon=new ImageIcon(getClass().getResource("/Images/undo.jpg"));
		 _undo = putIconOnButtton(undoIcon,1);
		listener.setUndo(_undo);
		_undo.addActionListener(listener);
		
		 ImageIcon hintIcon=new ImageIcon(getClass().getResource("/Images/help.png"));
		 _hint = putIconOnButtton(hintIcon,1);
		listener.setHint(_hint);
		_hint.addActionListener(listener);

		_timePassedLabel = new JLabel("Time Passed: 0");
		Font f = _timePassedLabel.getFont();
		_timePassedLabel.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
		
		JLabel levelId = new JLabel("Level Id: "+_mainFrame.getPlayGame().getLevel().getLevelId());
		f = levelId.getFont();
		levelId.setFont(f.deriveFont(f.getStyle() | Font.BOLD));
		
		int bestTimeint = _mainFrame.getPlayGame().getLevel().getBestTime();
		JLabel bestTime;
		if (bestTimeint >= 999999)
			bestTime = new JLabel("Best time: never finished!");
		else bestTime = new JLabel("Best time: "+bestTimeint);
		f = bestTime.getFont();
		bestTime.setFont(f.deriveFont(f.getStyle() | Font.BOLD));

		add(levelId);
		add(_back);
		add(_undo);
		add(_hint);
		add(_timePassedLabel);
		add(bestTime);


	}
	
	//putting a background _image for the JPanel
	public void paintComponent(Graphics g){
		try {
			_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSB.jpg"));
		}
		catch (Exception e){
			throw new RuntimeException("unable to load menu _image");
		}
		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);

	}
	
	//putting a background _image for the buttons
    private JButton putIconOnButtton(ImageIcon icon, int size){
        int unit = 50;
        Dimension d=new Dimension(unit*size,unit);
        icon = new ImageIcon(icon.getImage().getScaledInstance(unit*size, unit, java.awt.Image.SCALE_SMOOTH));
        JButton tmp =new JButton("",icon);
        tmp.setPreferredSize(d);
        return tmp;
    }
    
	public void stopTheTimer(){
		_t.stop();
	}
	public void startTheTimer(){
		_t.start();
	}
	
	//incase level completed
	public void won(){
		_mainFrame.setWinPage(_timepassed, _mainFrame.getPlayGame().getLevel().getBestTime());
		
	}
	public Game getGame(){
		return _mainFrame;
	}
	
}
