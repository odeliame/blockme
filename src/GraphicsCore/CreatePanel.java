package GraphicsCore;

import Listeners.levelBuildingListener;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;

/**
 * this class create the upper panel in the
 * "create new level" window.
 */
public class CreatePanel extends JPanel {
    JButton _peakmatorV;
    JButton _peakMackV;
    JButton _peakmator;
    JButton _peakMack;
    JButton _deleteFromBoard;
    JButton _doneButton;
    private BufferedImage _image;


    /**
     * constructor
     * @param buildingListener
     */
    public CreatePanel(levelBuildingListener buildingListener) {
        setLayout(new FlowLayout());
        this.setSize(600,25);

        //creates buttons

        //vertical cars buttons
        ImageIcon matorIcon, mackIcon;
        try{
            Image matorImage = ImageIO.read(this.getClass().getResourceAsStream("/Images/Materwb.jpg"));
            matorIcon=new ImageIcon(matorImage);
            Image mackImage = ImageIO.read(this.getClass().getResourceAsStream("/Images/mackwb.jpg"));
            mackIcon=new ImageIcon(mackImage);
        }catch (Exception e) {
            e.printStackTrace();
            return;
        }
        _peakmatorV = putIconOnButtton(matorIcon,true,2);
        buildingListener.set_peakmatorV(_peakmatorV);
        _peakmatorV.addActionListener(buildingListener);

        _peakMackV=putIconOnButtton(mackIcon,true,3);
        buildingListener.set_peakMackV(_peakMackV);
        _peakMackV.addActionListener(buildingListener);

        //horizontal buttons
        _peakmator = putIconOnButtton(matorIcon,false,2);
        buildingListener.set_peakmator(_peakmator);
        _peakmator.addActionListener(buildingListener);

        _peakMack=putIconOnButtton(mackIcon,false,3);
        buildingListener.set_peakMack(_peakMack);
        _peakMack.addActionListener(buildingListener);

        //functional buttons
        _deleteFromBoard = new JButton("Delete Marked Truck");
        buildingListener.set_deleteFromBoard(_deleteFromBoard);
        _deleteFromBoard.addActionListener(buildingListener);

        _doneButton = new JButton("Done");
        buildingListener.set_doneButton(_doneButton);
        _doneButton.addActionListener(buildingListener);

        add(_peakMackV);
        add(_peakmatorV);
        add(_peakMack);
        add(_peakmator);
        add(_deleteFromBoard);
        add(_doneButton);

    }

    /**
     * paint the background
     * @param g
     */
    public void paintComponent(Graphics g){
        try {
            _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/chooseLevelSB.jpg"));
        }
        catch (Exception e){
            throw new RuntimeException("unable to load menu _image");
        }
        g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);

    }

    /**
     * rotate an icon  - using for vertical tracks buttons
     * (pictures are originally horizontal)
     * @param icon - the icon to rotate
     * @param button - the button to put on
     * @return
     */
    private ImageIcon rotateIcon(ImageIcon icon, JButton button) {
        int w = icon.getIconWidth();
        int h = icon.getIconHeight();
        int type = BufferedImage.TYPE_INT_RGB;  // other options, see api
        BufferedImage image = new BufferedImage(h, w, type);
        Graphics2D g2 = image.createGraphics();
        double x = (h - w)/2.0;
        double y = (w - h)/2.0;
        AffineTransform at = AffineTransform.getTranslateInstance(x, y);
        at.rotate(Math.toRadians(90), w/2.0, h/2.0);
        g2.drawImage(icon.getImage(), at, button);
        g2.dispose();
        return new ImageIcon(image);
    }

    /**
     * rotate Icon and put on button
     * @param icon - the icon to rotate
     * @param toRotate - if rotation is desirable
     * @param size - the size of the truck - 2 or 3 blocks long
     * @return
     */
    public JButton putIconOnButtton(ImageIcon icon, boolean toRotate, int size){
        int unit = 50;
        Dimension d=new Dimension(unit*size,unit);
        if (toRotate) {
            icon = rotateIcon(icon, _peakMackV);
            d=new Dimension(unit, unit*size);
            icon = new ImageIcon(icon.getImage().getScaledInstance(unit, unit*size, java.awt.Image.SCALE_SMOOTH));
        }
        else {
            icon = new ImageIcon(icon.getImage().getScaledInstance(unit*size, unit, java.awt.Image.SCALE_SMOOTH));
        }
        JButton tmp =new JButton("",icon);
        tmp.setPreferredSize(d);
        return tmp;
    }
}
