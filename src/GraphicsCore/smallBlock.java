package GraphicsCore;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

import logics.Block;

public class smallBlock extends JPanel{
	
	private int _row,_col;
	private Block _block;
	private Board _Board;
	BufferedImage _image;
	private boolean _marked;

	public smallBlock(Block block, int row, int col, Board Board, MouseListener listener){
		addMouseListener(listener);
		_Board=Board;
		_block=block;
		_marked=false;
		_row=row;
		_col=col;
		this.setSize(100, 100);
	    setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setVisible(true);
		
	}
	//putting a background _image for the JPanel, _image is selected by the paramaters row,col,block,marked
    public void paint(Graphics g) {
    	try {
			_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/BlackSquare.jpg"));
			if (_block!=null){
				if(_block.isTarget()) //if the block is the target
					if ((_row + _col)%2==1) //its a "chess board"
						if (_marked) //if we mark the block - click on it
							_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/speedywbMarked.jpg"));
						else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/speedywb.jpg"));
					else if (_marked) _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/speedybwMarked.jpg"));
						else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/speedybw.jpg"));
				else  if (_block.getSize()==2) //if block is a size of 2
							if ((_row + _col)%2==1)
								if (_block.isVertical()) //is the block vertical? if yes than rotate the _image 90 degrees
									if(_marked)
										_image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/MaterwbMarked.jpg")));
									else _image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/Materwb.jpg")));
								else if(_marked)
										_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/MaterwbMarked.jpg"));
									else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/Materwb.jpg"));
							else if (_block.isVertical())
									if(_marked)
										_image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/MaterbwMarked.jpg")));
									else _image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/Materbw.jpg")));
								else if(_marked)
										_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/MaterbwMarked.jpg"));
									else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/Materbw.jpg"));
				else if (_block.getSize()==3) //same for size 3 blocks as size 2 blocks
					if ((_row + _col)%2==1)
						if (_block.isVertical())
							if(_marked)
								_image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/mackwbMarked.jpg")));
							else _image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/mackwb.jpg")));
						else if(_marked)
								_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/mackwbMarked.jpg"));
							else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/mackwb.jpg"));
					else if (_block.isVertical())
							if(_marked)
								_image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/mackbwMarked.jpg")));
							else _image = rotateCw(ImageIO.read(this.getClass().getResourceAsStream("/Images/mackbw.jpg")));
						else if(_marked)
								_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/mackbwMarked.jpg"));
							else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/mackbw.jpg"));
				
			}
			else {
				if ((_row + _col)%2==1)
					_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/WhiteSquare.jpg"));
			}
		}
		catch (IOException e) {
			throw new RuntimeException("unable to reach graphic files");
		}

		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null); //draw the _image selected
	}
    
    //mark this block
	public void mark(boolean marked){
		_marked=marked;
		repaint();
		_Board.setClickedBlock(this);
	}

	//rotate the _image received 90 degrees clock wise
	private static BufferedImage rotateCw(BufferedImage img){
	    int width = img.getWidth();
	    int height = img.getHeight();
	    BufferedImage newImage = new BufferedImage( height, width, img.getType() );
	    for( int i=0 ; i < width ; i++ )
	        for( int j=0 ; j < height ; j++ )
	            newImage.setRGB( height-1-j, i, img.getRGB(i,j) );
	    return newImage;
	}
	
	public boolean isMarked(){
		return _marked;
	}
	public void setMarked(boolean marked){
		_marked=marked;
		repaint();
	}
	public Block getBlock() {
		return _block;
	}
	public void setBlock(Block block) {
		this._block = block;
	}
	public int getRow() {
		return _row;
	}
	public void setRow(int row) {
		this._row = row;
	}
	public int getCol() {
		return _col;
	}
	public void setCol(int col) {
		this._col = col;
	}

}
