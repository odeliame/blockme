package GraphicsCore;
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import Listeners.playingListener;
import logics.Block;
import logics.PlayGame;

public class Board extends JPanel{
	private GridBagLayout _gbl;
	private smallBlock _clickedSmallBlock;
	private boolean _firstMovePlayed;
	private Game _mainFrame;
	private Block[][] _logicBoard;
	private smallBlock[][] smallBlocks;
	private MouseListener mouseListener;
	
	//using this constructor for creating a playing board
	public Board(Game mainFrame, playingListener listener, KeyListener kListener){
		this(mainFrame.getPlayGame().getBoard(),listener);
		_mainFrame=mainFrame;
        addKeyListener(kListener);
		
	}
	
	//going straight to this constructor for creating a board for create Level
	public Board(Block[][] logicBoard,MouseListener mListener){
		mouseListener =mListener;
		_firstMovePlayed=false;
		smallBlocks = new smallBlock[logicBoard.length][logicBoard.length];
	    setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    this.setVisible(true);
	    this.setSize(600, 600);
	    
        _gbl = new GridBagLayout();
		setLayout(_gbl);
        _gbl.rowWeights = new double[] { 0.1, 0.1, 0.1, 0.1, 0.1,0.1,0.1 };
        _gbl.columnWeights = new double[] { 0.1, 0.1, 0.1, 0.1, 0.1,0.1, 0.00001};
        createBoard(logicBoard,mListener);
	}
	
	//this function receives a double block array from the logic part and creates a visual board made of smallBlock JPanels
	public void createBoard(Block[][] logicBoard,MouseListener listener){
		this.setFocusable(true);
		this.requestFocus();
		this.setLogicBoard(logicBoard);
		GridBagConstraints gbc = new GridBagConstraints(0,0,1,1,0.0, 0.0, GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(0, 0, 0, 0), 0, 0);
		for (int i=0;i<logicBoard.length;i++){
			for (int j=0;j<logicBoard[i].length;j++){
				int gridX=j;
				int gridY=i;
				int gridWidth=1;
				int gridHeight=1;
				smallBlock sB = new smallBlock(logicBoard[i][j],i,j,this,listener);
				if (i>0 && logicBoard[i][j]!=null){
					if (logicBoard[i-1][j]==logicBoard[i][j]){
						continue;
					}
				}
				if (j>0 && logicBoard[i][j]!=null){
					if (logicBoard[i][j-1]==logicBoard[i][j])
						continue;
				}


				if (logicBoard[i][j]!=null){
					if (logicBoard[i][j].isVertical())
						gridHeight = logicBoard[i][j].getSize();
					else 
						gridWidth = logicBoard[i][j].getSize();
				}
				gbc.gridx=gridX;
				gbc.gridy=gridY;
				gbc.gridwidth=gridWidth;
				gbc.gridheight=gridHeight;
				add(sB,gbc);

				sB.setBorder(BorderFactory.createLineBorder(Color.BLACK));
				smallBlocks[i][j] = sB;
			}

			separationBuffer metalPanel = new separationBuffer(i);
			gbc.gridx=6;
			gbc.gridy=i;
			gbc.gridwidth=1;
			gbc.gridheight=1;
			add(metalPanel,gbc);
			metalPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
		}
	}
		

	//this function recreates a board if a move is played
	public void updateBoard(){
		try {
		   removeAll();
		   revalidate();
		   createBoard(_mainFrame.getPlayGame().getBoard(), mouseListener);
		}
		catch (Exception e){
			throw new RuntimeException("error at updateBoard");
		}
	}

	public smallBlock getClickedBlock() {
		return _clickedSmallBlock;
	}

	//setClickedBlock is responsible for saving the proper clicked box
	public void setClickedBlock(smallBlock clickedSmallBlock) {
		if (clickedSmallBlock!=_clickedSmallBlock){
			if (_clickedSmallBlock!=null)
				_clickedSmallBlock.setMarked(false);
			_clickedSmallBlock = clickedSmallBlock;
		}
		else _clickedSmallBlock=null;
		
	}

	public PlayGame getPlayGame(){
		return _mainFrame.getPlayGame();
	}

	public Block[][] getLogicBoard() {
		return _logicBoard;
	}
	
	public smallBlock getClickedSmallBlock(){
		return _clickedSmallBlock;
	}

	public void setClickedSmallBlock(smallBlock clickedSmallBlock){
		_clickedSmallBlock=clickedSmallBlock;
	}
	
	public void setLogicBoard(Block[][] logicBoard) {
		this._logicBoard = logicBoard;
	}
	public smallBlock[][] getSmallBlocks() {
		return smallBlocks;
	}
	public void setSmallBlocks(smallBlock[][] smallBlocks) {
		this.smallBlocks = smallBlocks;
	}
	public boolean isFirstMovePlayed(){
		return _firstMovePlayed;
	}
	public void setFirstMovePlayed(boolean played){
		_firstMovePlayed=played;
	}
}
