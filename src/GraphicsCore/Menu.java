package GraphicsCore;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

import Listeners.menuListener;


public class Menu extends JPanel{
	private JButton _play;
	private JButton _createLevels;
	private JButton _exit;
	private BufferedImage _image;
	private Game _frame;
	public Menu(Game frame){
		_frame=frame;
	    setSize(600, 660);
		setVisible(true);
		setLayout(null);
		_play = new JButton();
		_play.setBounds((int) (getWidth()*0.26), (int) (getHeight()*0.65), (int) (getWidth()*0.25), (int) (getWidth()*0.17));
		_play.setOpaque(false);//"invisible button"
		_play.setContentAreaFilled(false);//"invisible button"
		_play.setBorderPainted(false);//"invisible button"
		_play.setVisible(true);

		
		_createLevels = new JButton();
		_createLevels.setBounds((int) (getWidth()*0.76), (int) (getHeight()*0.52), (int) (getWidth()*0.17), (int) (getWidth()*0.17));
		_createLevels.setOpaque(false);//"invisible button"
		_createLevels.setContentAreaFilled(false); //"invisible button"
		_createLevels.setBorderPainted(false);//"invisible button"
		_createLevels.setVisible(true);
		
		_exit = new JButton();
		_exit.setBounds((int) (getWidth()*0.1), (int) (getHeight()*0.14), (int) (getWidth()*0.17), (int) (getWidth()*0.13));
		_exit.setOpaque(false);//"invisible button"
		_exit.setContentAreaFilled(false); //"invisible button"
		_exit.setBorderPainted(false);//"invisible button"
		_exit.setVisible(true);

		menuListener listener = new menuListener(_play,_createLevels,_frame);
		listener.setExitButton(_exit);
		_play.addActionListener(listener);
		_createLevels.addActionListener(listener);
		_exit.addActionListener(listener);

		add(_play);
		add(_createLevels);
		add(_exit);

	}
    
	//putting a background _image for the JPanel
	public void paintComponent(Graphics g){
		try {

			_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/Menu2.jpg"));
		}
		catch (Exception e){
			throw new RuntimeException("unable to load menu _image");
		}
		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);

	}
	

}

