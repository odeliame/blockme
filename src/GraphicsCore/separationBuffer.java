package GraphicsCore;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class separationBuffer extends JPanel {
	private int _row;
	BufferedImage _image;
	public separationBuffer(int row){
		_row=row;
		this.setSize(10, 100);
	    setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setVisible(true);
	}
	public void paint(Graphics g){
		try {
			if (_row!=2)
				_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/metalPanel.jpg"));
			else _image = ImageIO.read(this.getClass().getResourceAsStream("/Images/red.jpg"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);

	}
}
