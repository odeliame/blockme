package GraphicsCore;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.JPanel;
import Listeners.menuListener;
import logics.Level;

public class chooseLevel extends JPanel{
	
	private GridBagLayout _gbl;
	private GridBagConstraints _gbc;
	private Game _frame;
	private final int numOfElementsInARow=5;
	public chooseLevel(Game frame){
		_frame=frame;
	    setBorder(BorderFactory.createLineBorder(Color.BLACK));
	    this.setVisible(true);
	    this.setSize(595,620);
        _gbl = new GridBagLayout();
		setLayout(_gbl);
        _gbl.rowWeights = new double[] { 0.1, 0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1 };
        _gbl.columnWeights = new double[] { 0.1, 0.1, 0.1, 0.1, 0.1,0.1,0.1,0.1,0.1,0.1,0.1};
        _gbc = new GridBagConstraints(0,0,1,1,0.0, 0.0, GridBagConstraints.CENTER,GridBagConstraints.BOTH,new Insets(10, 10, 10, 10), 0, 0);
			showLevels(logics.Data.loadLevels());

	}
	
	//show all levels, each level in his own square
	public void showLevels(HashMap<Integer,Level> allLevels){
		menuListener listener = new menuListener(_frame);
		for (int i=0;i<allLevels.size();i++){
			levelSmallBlock lsb = new levelSmallBlock(allLevels.get(i), listener);
			_gbc.gridx=i%numOfElementsInARow;;
			_gbc.gridy=i/numOfElementsInARow;
			add(lsb,_gbc);
		}
		//add empty slots - if player passes this slots additional slots are automatically added
		for (int i=allLevels.size();i<22;i++){
			levelSmallBlock lsb = new levelSmallBlock(null, listener);
			_gbc.gridx=i%numOfElementsInARow;;
			_gbc.gridy=i/numOfElementsInARow;
			lsb.setVisible(true);
			add(lsb,_gbc);
		}
	}
	
}

