package GraphicsCore;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Listeners.menuListener;
import logics.Level;

public class winningPage extends JPanel{
	private Game _frame;
	private BufferedImage _image;
	private JButton _nextLevel;
	private JButton _chooseLevels;
	private JButton _MainMenu;
	public winningPage(Game frame, int playedTime, Level level){
		_frame=frame;
		_frame.getPlayGame().saveLevel(playedTime);	
		this.setVisible(true);
		this.setSize(595,620);
		setLayout(null);
		
		Integer shortPlayedTime = new Integer(playedTime);
		Integer shortBestTime = new Integer(level.getBestTime());
		if (level.getBestTime()>playedTime | level.getBestTime()==-1)
			shortBestTime=shortPlayedTime;
		JLabel results = new JLabel("<html>Time Played: "+shortPlayedTime.shortValue()+ "<br>" +"Record Time: "+shortBestTime.shortValue()+"</html>");
		results.setBounds((int) (getWidth()*0.58), (int) (getHeight()*0.61), (int) (getWidth()*0.2), (int) (getWidth()*0.3));
		
		_nextLevel = new JButton();
		_nextLevel.setBounds((int) (getWidth()*0.85), (int) (getHeight()*0.4), (int) (getWidth()*0.12), (int) (getWidth()*0.17));
		_nextLevel.setOpaque(false);//"invisible button"
		_nextLevel.setContentAreaFilled(false);//"invisible button"
		_nextLevel.setBorderPainted(false);//"invisible button"
		_nextLevel.setVisible(true);

		_MainMenu = new JButton();
		_MainMenu.setBounds((int) (getWidth()*0.05), (int) (getHeight()*0.35), (int) (getWidth()*0.14), (int) (getWidth()*0.17));
		_MainMenu.setOpaque(false);//"invisible button"
		_MainMenu.setContentAreaFilled(false);//"invisible button"
		_MainMenu.setBorderPainted(false);//"invisible button"
		_MainMenu.setVisible(true);
		
		_chooseLevels = new JButton();
		_chooseLevels.setBounds((int) (getWidth()*0.23), (int) (getHeight()*0.45), (int) (getWidth()*0.17), (int) (getWidth()*0.17));
		_chooseLevels.setOpaque(false);//"invisible button"
		_chooseLevels.setContentAreaFilled(false);//"invisible button"
		_chooseLevels.setBorderPainted(false);//"invisible button"
		_chooseLevels.setVisible(true);
		
		menuListener listener = new menuListener(_MainMenu,_chooseLevels,_nextLevel,_frame);
		_nextLevel.addActionListener(listener);
		_MainMenu.addActionListener(listener);
		_chooseLevels.addActionListener(listener);
		
		add(_nextLevel);
		add(_MainMenu);
		add(_chooseLevels);
		add(results);
	}
	
	//putting a background _image for the JPanel
	public void paintComponent(Graphics g){
		try {
			_image = ImageIO.read(this.getClass().getResourceAsStream("/Images/Win2.jpg"));
		}
		catch (Exception e){
			throw new RuntimeException("unable to load Win _image");
		}
		g.drawImage(_image, 0, 0, this.getWidth(), this.getHeight(), null);

	}
}
